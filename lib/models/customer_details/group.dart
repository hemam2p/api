class Group {
  int? id;
  String? name;
  dynamic createdAt;
  dynamic updatedAt;

  Group({this.id, this.name, this.createdAt, this.updatedAt});

  factory Group.fromJson(Map<String, dynamic> json) => Group(
        id: json['id'] as int?,
        name: json['name'] as String?,
        createdAt: json['created_at'] as dynamic?,
        updatedAt: json['updated_at'] as dynamic?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'created_at': createdAt,
        'updated_at': updatedAt,
      };
}
