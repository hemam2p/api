import 'cart.dart';
import 'rate.dart';

class Data {
  List<Rate>? rates;
  Cart? cart;

  Data({this.rates, this.cart});

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        rates: (json['rates'] as List<dynamic>?)
            ?.map((e) => Rate.fromJson(e as Map<String, dynamic>))
            .toList(),
        cart: json['cart'] == null
            ? null
            : Cart.fromJson(json['cart'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => {
        'rates': rates?.map((e) => e.toJson()).toList(),
        'cart': cart?.toJson(),
      };
}
