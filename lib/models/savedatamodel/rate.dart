import 'rate.dart';

class Rate {
  String? carrierTitle;
  List<Rate>? rates;

  Rate({this.carrierTitle, this.rates});

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        carrierTitle: json['carrier_title'] as String?,
        rates: (json['rates'] as List<dynamic>?)
            ?.map((e) => Rate.fromJson(e as Map<String, dynamic>))
            .toList(),
      );

  Map<String, dynamic> toJson() => {
        'carrier_title': carrierTitle,
        'rates': rates?.map((e) => e.toJson()).toList(),
      };
}
