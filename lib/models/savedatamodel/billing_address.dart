class BillingAddress {
  int? id;
  String? firstName;
  String? lastName;
  String? name;
  String? email;
  String? address1;
  String? landmark;
  String? country;
  String? countryName;
  String? state;
  String? city;
  String? postcode;
  String? phone;
  String? createdAt;
  String? updatedAt;

  BillingAddress({
    this.id,
    this.firstName,
    this.lastName,
    this.name,
    this.email,
    this.address1,
    this.landmark,
    this.country,
    this.countryName,
    this.state,
    this.city,
    this.postcode,
    this.phone,
    this.createdAt,
    this.updatedAt,
  });

  factory BillingAddress.fromJson(Map<String, dynamic> json) {
    return BillingAddress(
      id: json['id'] as int?,
      firstName: json['first_name'] as String?,
      lastName: json['last_name'] as String?,
      name: json['name'] as String?,
      email: json['email'] as String?,
      address1: json['address1'] as String?,
      landmark: json['landmark'] as String?,
      country: json['country'] as String?,
      countryName: json['country_name'] as String?,
      state: json['state'] as String?,
      city: json['city'] as String?,
      postcode: json['postcode'] as String?,
      phone: json['phone'] as String?,
      createdAt: json['created_at'] as String?,
      updatedAt: json['updated_at'] as String?,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'first_name': firstName,
        'last_name': lastName,
        'name': name,
        'email': email,
        'address1': address1,
        'landmark': landmark,
        'country': country,
        'country_name': countryName,
        'state': state,
        'city': city,
        'postcode': postcode,
        'phone': phone,
        'created_at': createdAt,
        'updated_at': updatedAt,
      };
}
