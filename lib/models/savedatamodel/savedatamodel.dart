import 'data.dart';

class Savedatamodel {
  Data? data;

  Savedatamodel({this.data});

  factory Savedatamodel.fromJson(Map<String, dynamic> json) => Savedatamodel(
        data: json['data'] == null
            ? null
            : Data.fromJson(json['data'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => {
        'data': data?.toJson(),
      };
}
