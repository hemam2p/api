import 'additional.dart';
import 'product.dart';

class Item {
  int? id;
  int? quantity;
  String? sku;
  String? type;
  String? name;
  dynamic couponCode;
  String? weight;
  String? totalWeight;
  String? baseTotalWeight;
  String? price;
  String? formatedPrice;
  String? basePrice;
  String? formatedBasePrice;
  dynamic customPrice;
  String? formatedCustomPrice;
  String? total;
  String? formatedTotal;
  String? baseTotal;
  String? formatedBaseTotal;
  String? taxPercent;
  String? taxAmount;
  String? formatedTaxAmount;
  String? baseTaxAmount;
  String? formatedBaseTaxAmount;
  String? discountPercent;
  String? discountAmount;
  String? formatedDiscountAmount;
  String? baseDiscountAmount;
  String? formatedBaseDiscountAmount;
  Additional? additional;
  dynamic child;
  Product? product;
  String? createdAt;
  String? updatedAt;

  Item({
    this.id,
    this.quantity,
    this.sku,
    this.type,
    this.name,
    this.couponCode,
    this.weight,
    this.totalWeight,
    this.baseTotalWeight,
    this.price,
    this.formatedPrice,
    this.basePrice,
    this.formatedBasePrice,
    this.customPrice,
    this.formatedCustomPrice,
    this.total,
    this.formatedTotal,
    this.baseTotal,
    this.formatedBaseTotal,
    this.taxPercent,
    this.taxAmount,
    this.formatedTaxAmount,
    this.baseTaxAmount,
    this.formatedBaseTaxAmount,
    this.discountPercent,
    this.discountAmount,
    this.formatedDiscountAmount,
    this.baseDiscountAmount,
    this.formatedBaseDiscountAmount,
    this.additional,
    this.child,
    this.product,
    this.createdAt,
    this.updatedAt,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json['id'] as int?,
        quantity: json['quantity'] as int?,
        sku: json['sku'] as String?,
        type: json['type'] as String?,
        name: json['name'] as String?,
        couponCode: json['coupon_code'] as dynamic,
        weight: json['weight'] as String?,
        totalWeight: json['total_weight'] as String?,
        baseTotalWeight: json['base_total_weight'] as String?,
        price: json['price'] as String?,
        formatedPrice: json['formated_price'] as String?,
        basePrice: json['base_price'] as String?,
        formatedBasePrice: json['formated_base_price'] as String?,
        customPrice: json['custom_price'] as dynamic,
        formatedCustomPrice: json['formated_custom_price'] as String?,
        total: json['total'] as String?,
        formatedTotal: json['formated_total'] as String?,
        baseTotal: json['base_total'] as String?,
        formatedBaseTotal: json['formated_base_total'] as String?,
        taxPercent: json['tax_percent'] as String?,
        taxAmount: json['tax_amount'] as String?,
        formatedTaxAmount: json['formated_tax_amount'] as String?,
        baseTaxAmount: json['base_tax_amount'] as String?,
        formatedBaseTaxAmount: json['formated_base_tax_amount'] as String?,
        discountPercent: json['discount_percent'] as String?,
        discountAmount: json['discount_amount'] as String?,
        formatedDiscountAmount: json['formated_discount_amount'] as String?,
        baseDiscountAmount: json['base_discount_amount'] as String?,
        formatedBaseDiscountAmount:
            json['formated_base_discount_amount'] as String?,
        additional: json['additional'] == null
            ? null
            : Additional.fromJson(json['additional'] as Map<String, dynamic>),
        child: json['child'] as dynamic,
        product: json['product'] == null
            ? null
            : Product.fromJson(json['product'] as Map<String, dynamic>),
        createdAt: json['created_at'] as String?,
        updatedAt: json['updated_at'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'quantity': quantity,
        'sku': sku,
        'type': type,
        'name': name,
        'coupon_code': couponCode,
        'weight': weight,
        'total_weight': totalWeight,
        'base_total_weight': baseTotalWeight,
        'price': price,
        'formated_price': formatedPrice,
        'base_price': basePrice,
        'formated_base_price': formatedBasePrice,
        'custom_price': customPrice,
        'formated_custom_price': formatedCustomPrice,
        'total': total,
        'formated_total': formatedTotal,
        'base_total': baseTotal,
        'formated_base_total': formatedBaseTotal,
        'tax_percent': taxPercent,
        'tax_amount': taxAmount,
        'formated_tax_amount': formatedTaxAmount,
        'base_tax_amount': baseTaxAmount,
        'formated_base_tax_amount': formatedBaseTaxAmount,
        'discount_percent': discountPercent,
        'discount_amount': discountAmount,
        'formated_discount_amount': formatedDiscountAmount,
        'base_discount_amount': baseDiscountAmount,
        'formated_base_discount_amount': formatedBaseDiscountAmount,
        'additional': additional?.toJson(),
        'child': child,
        'product': product?.toJson(),
        'created_at': createdAt,
        'updated_at': updatedAt,
      };
}
