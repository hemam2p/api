class Additional {
  int? quantity;
  String? productId;
  String? token;

  Additional({this.quantity, this.productId, this.token});

  factory Additional.fromJson(Map<String, dynamic> json) => Additional(
        quantity: json['quantity'] as int?,
        productId: json['product_id'] as String?,
        token: json['token'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'quantity': quantity,
        'product_id': productId,
        'token': token,
      };
}
