class BaseImage {
  String? smallImageUrl;
  String? mediumImageUrl;
  String? largeImageUrl;
  String? originalImageUrl;

  BaseImage({
    this.smallImageUrl,
    this.mediumImageUrl,
    this.largeImageUrl,
    this.originalImageUrl,
  });

  factory BaseImage.fromJson(Map<String, dynamic> json) => BaseImage(
        smallImageUrl: json['small_image_url'] as String?,
        mediumImageUrl: json['medium_image_url'] as String?,
        largeImageUrl: json['large_image_url'] as String?,
        originalImageUrl: json['original_image_url'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'small_image_url': smallImageUrl,
        'medium_image_url': mediumImageUrl,
        'large_image_url': largeImageUrl,
        'original_image_url': originalImageUrl,
      };
}
