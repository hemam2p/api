import 'billing_address.dart';
import 'item.dart';
import 'shipping_address.dart';

class Cart {
  int? id;
  String? customerEmail;
  String? customerFirstName;
  String? customerLastName;
  dynamic shippingMethod;
  dynamic couponCode;
  int? isGift;
  int? itemsCount;
  String? itemsQty;
  dynamic exchangeRate;
  String? globalCurrencyCode;
  String? baseCurrencyCode;
  String? channelCurrencyCode;
  String? cartCurrencyCode;
  String? grandTotal;
  String? formatedGrandTotal;
  String? baseGrandTotal;
  String? formatedBaseGrandTotal;
  String? subTotal;
  String? formatedSubTotal;
  String? baseSubTotal;
  String? formatedBaseSubTotal;
  String? taxTotal;
  String? formatedTaxTotal;
  String? baseTaxTotal;
  String? formatedBaseTaxTotal;
  String? discount;
  String? formatedDiscount;
  String? baseDiscount;
  String? formatedBaseDiscount;
  dynamic checkoutMethod;
  int? isGuest;
  int? isActive;
  dynamic conversionTime;
  dynamic customer;
  dynamic channel;
  List<Item>? items;
  dynamic selectedShippingRate;
  dynamic payment;
  BillingAddress? billingAddress;
  ShippingAddress? shippingAddress;
  dynamic userRelations;
  String? createdAt;
  dynamic shippingDate;
  String? updatedAt;
  String? taxes;
  String? formatedTaxes;
  String? baseTaxes;
  String? formatedBaseTaxes;
  String? formatedDiscountedSubTotal;
  String? formatedBaseDiscountedSubTotal;

  Cart({
    this.id,
    this.customerEmail,
    this.customerFirstName,
    this.customerLastName,
    this.shippingMethod,
    this.couponCode,
    this.isGift,
    this.itemsCount,
    this.itemsQty,
    this.exchangeRate,
    this.globalCurrencyCode,
    this.baseCurrencyCode,
    this.channelCurrencyCode,
    this.cartCurrencyCode,
    this.grandTotal,
    this.formatedGrandTotal,
    this.baseGrandTotal,
    this.formatedBaseGrandTotal,
    this.subTotal,
    this.formatedSubTotal,
    this.baseSubTotal,
    this.formatedBaseSubTotal,
    this.taxTotal,
    this.formatedTaxTotal,
    this.baseTaxTotal,
    this.formatedBaseTaxTotal,
    this.discount,
    this.formatedDiscount,
    this.baseDiscount,
    this.formatedBaseDiscount,
    this.checkoutMethod,
    this.isGuest,
    this.isActive,
    this.conversionTime,
    this.customer,
    this.channel,
    this.items,
    this.selectedShippingRate,
    this.payment,
    this.billingAddress,
    this.shippingAddress,
    this.userRelations,
    this.createdAt,
    this.shippingDate,
    this.updatedAt,
    this.taxes,
    this.formatedTaxes,
    this.baseTaxes,
    this.formatedBaseTaxes,
    this.formatedDiscountedSubTotal,
    this.formatedBaseDiscountedSubTotal,
  });

  factory Cart.fromJson(Map<String, dynamic> json) => Cart(
        id: json['id'] as int?,
        customerEmail: json['customer_email'] as String?,
        customerFirstName: json['customer_first_name'] as String?,
        customerLastName: json['customer_last_name'] as String?,
        shippingMethod: json['shipping_method'] as dynamic,
        couponCode: json['coupon_code'] as dynamic,
        isGift: json['is_gift'] as int?,
        itemsCount: json['items_count'] as int?,
        itemsQty: json['items_qty'] as String?,
        exchangeRate: json['exchange_rate'] as dynamic,
        globalCurrencyCode: json['global_currency_code'] as String?,
        baseCurrencyCode: json['base_currency_code'] as String?,
        channelCurrencyCode: json['channel_currency_code'] as String?,
        cartCurrencyCode: json['cart_currency_code'] as String?,
        grandTotal: json['grand_total'] as String?,
        formatedGrandTotal: json['formated_grand_total'] as String?,
        baseGrandTotal: json['base_grand_total'] as String?,
        formatedBaseGrandTotal: json['formated_base_grand_total'] as String?,
        subTotal: json['sub_total'] as String?,
        formatedSubTotal: json['formated_sub_total'] as String?,
        baseSubTotal: json['base_sub_total'] as String?,
        formatedBaseSubTotal: json['formated_base_sub_total'] as String?,
        taxTotal: json['tax_total'] as String?,
        formatedTaxTotal: json['formated_tax_total'] as String?,
        baseTaxTotal: json['base_tax_total'] as String?,
        formatedBaseTaxTotal: json['formated_base_tax_total'] as String?,
        discount: json['discount'] as String?,
        formatedDiscount: json['formated_discount'] as String?,
        baseDiscount: json['base_discount'] as String?,
        formatedBaseDiscount: json['formated_base_discount'] as String?,
        checkoutMethod: json['checkout_method'] as dynamic,
        isGuest: json['is_guest'] as int?,
        isActive: json['is_active'] as int?,
        conversionTime: json['conversion_time'] as dynamic,
        customer: json['customer'] as dynamic,
        channel: json['channel'] as dynamic,
        items: (json['items'] as List<dynamic>?)
            ?.map((e) => Item.fromJson(e as Map<String, dynamic>))
            .toList(),
        selectedShippingRate: json['selected_shipping_rate'] as dynamic,
        payment: json['payment'] as dynamic,
        billingAddress: json['billing_address'] == null
            ? null
            : BillingAddress.fromJson(
                json['billing_address'] as Map<String, dynamic>),
        shippingAddress: json['shipping_address'] == null
            ? null
            : ShippingAddress.fromJson(
                json['shipping_address'] as Map<String, dynamic>),
        userRelations: json['user_relations'] as dynamic,
        createdAt: json['created_at'] as String?,
        shippingDate: json['shipping_date'] as dynamic,
        updatedAt: json['updated_at'] as String?,
        taxes: json['taxes'] as String?,
        formatedTaxes: json['formated_taxes'] as String?,
        baseTaxes: json['base_taxes'] as String?,
        formatedBaseTaxes: json['formated_base_taxes'] as String?,
        formatedDiscountedSubTotal:
            json['formated_discounted_sub_total'] as String?,
        formatedBaseDiscountedSubTotal:
            json['formated_base_discounted_sub_total'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'customer_email': customerEmail,
        'customer_first_name': customerFirstName,
        'customer_last_name': customerLastName,
        'shipping_method': shippingMethod,
        'coupon_code': couponCode,
        'is_gift': isGift,
        'items_count': itemsCount,
        'items_qty': itemsQty,
        'exchange_rate': exchangeRate,
        'global_currency_code': globalCurrencyCode,
        'base_currency_code': baseCurrencyCode,
        'channel_currency_code': channelCurrencyCode,
        'cart_currency_code': cartCurrencyCode,
        'grand_total': grandTotal,
        'formated_grand_total': formatedGrandTotal,
        'base_grand_total': baseGrandTotal,
        'formated_base_grand_total': formatedBaseGrandTotal,
        'sub_total': subTotal,
        'formated_sub_total': formatedSubTotal,
        'base_sub_total': baseSubTotal,
        'formated_base_sub_total': formatedBaseSubTotal,
        'tax_total': taxTotal,
        'formated_tax_total': formatedTaxTotal,
        'base_tax_total': baseTaxTotal,
        'formated_base_tax_total': formatedBaseTaxTotal,
        'discount': discount,
        'formated_discount': formatedDiscount,
        'base_discount': baseDiscount,
        'formated_base_discount': formatedBaseDiscount,
        'checkout_method': checkoutMethod,
        'is_guest': isGuest,
        'is_active': isActive,
        'conversion_time': conversionTime,
        'customer': customer,
        'channel': channel,
        'items': items?.map((e) => e.toJson()).toList(),
        'selected_shipping_rate': selectedShippingRate,
        'payment': payment,
        'billing_address': billingAddress?.toJson(),
        'shipping_address': shippingAddress?.toJson(),
        'user_relations': userRelations,
        'created_at': createdAt,
        'shipping_date': shippingDate,
        'updated_at': updatedAt,
        'taxes': taxes,
        'formated_taxes': formatedTaxes,
        'base_taxes': baseTaxes,
        'formated_base_taxes': formatedBaseTaxes,
        'formated_discounted_sub_total': formatedDiscountedSubTotal,
        'formated_base_discounted_sub_total': formatedBaseDiscountedSubTotal,
      };
}
