class SamplePostApiModel {
  int? id;

  SamplePostApiModel({this.id});

  factory SamplePostApiModel.fromJson(Map<String, dynamic> json) {
    return SamplePostApiModel(
      id: json['id'] as int?,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
      };
}
