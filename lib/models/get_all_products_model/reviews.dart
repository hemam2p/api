class Reviews {
  int? total;
  int? totalRating;
  int? averageRating;
  List<dynamic>? percentage;

  Reviews({
    this.total,
    this.totalRating,
    this.averageRating,
    this.percentage,
  });

  factory Reviews.fromJson(Map<String, dynamic> json) => Reviews(
        total: json['total'] as int?,
        totalRating: json['total_rating'] as int?,
        averageRating: json['average_rating'] as int?,
        percentage: json['percentage'] as List<dynamic>?,
      );

  Map<String, dynamic> toJson() => {
        'total': total,
        'total_rating': totalRating,
        'average_rating': averageRating,
        'percentage': percentage,
      };
}
