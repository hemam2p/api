import 'base_image.dart';
import 'reviews.dart';

class Datum {
  int? id;
  String? sku;
  String? type;
  String? name;
  String? urlKey;
  String? price;
  String? formatedPrice;
  String? shortDescription;
  String? description;
  List<dynamic>? images;
  List<dynamic>? videos;
  BaseImage? baseImage;
  String? createdAt;
  String? updatedAt;

  String? interpretation;
  String? prerequisites;
  dynamic serviceType;
  dynamic serviceId;
  String? sampletype;
  String? departmentCode;
  String? department;
  Reviews? reviews;
  bool? inStock;
  bool? isSaved;
  bool? isWishlisted;
  bool? isItemInCart;
  bool? showQuantityChanger;

  Datum({
    this.id,
    this.sku,
    this.type,
    this.name,
    this.urlKey,
    this.price,
    this.formatedPrice,
    this.shortDescription,
    this.description,
    this.images,
    this.videos,
    this.baseImage,
    this.createdAt,
    this.updatedAt,
    this.interpretation,
    this.prerequisites,
    this.serviceType,
    this.serviceId,
    this.sampletype,
    this.departmentCode,
    this.department,
    this.reviews,
    this.inStock,
    this.isSaved,
    this.isWishlisted,
    this.isItemInCart,
    this.showQuantityChanger,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json['id'] as int?,
        sku: json['sku'] as String?,
        type: json['type'] as String?,
        name: json['name'] as String?,
        urlKey: json['url_key'] as String?,
        price: json['price'] as String?,
        formatedPrice: json['formated_price'] as String?,
        shortDescription: json['short_description'] as String?,
        description: json['description'] as String?,
        images: json['images'] as List<dynamic>?,
        videos: json['videos'] as List<dynamic>?,
        baseImage: json['base_image'] == null
            ? null
            : BaseImage.fromJson(json['base_image'] as Map<String, dynamic>),
        createdAt: json['created_at'] as String?,
        updatedAt: json['updated_at'] as String?,
        interpretation: json['interpretation'] as String?,
        prerequisites: json['prerequisites'] as String?,
        serviceType: json['service_type'] as dynamic,
        serviceId: json['service_id'] as dynamic,
        sampletype: json['sampletype'] as String?,
        departmentCode: json['department_code'] as String?,
        department: json['department'] as String?,
        reviews: json['reviews'] == null
            ? null
            : Reviews.fromJson(json['reviews'] as Map<String, dynamic>),
        inStock: json['in_stock'] as bool?,
        isSaved: json['is_saved'] as bool?,
        isWishlisted: json['is_wishlisted'] as bool?,
        isItemInCart: json['is_item_in_cart'] as bool?,
        showQuantityChanger: json['show_quantity_changer'] as bool?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'sku': sku,
        'type': type,
        'name': name,
        'url_key': urlKey,
        'price': price,
        'formated_price': formatedPrice,
        'short_description': shortDescription,
        'description': description,
        'images': images,
        'videos': videos,
        'base_image': baseImage?.toJson(),
        'created_at': createdAt,
        'updated_at': updatedAt,
        'shortDescription': shortDescription,
        'interpretation': interpretation,
        'prerequisites': prerequisites,
        'service_type': serviceType,
        'service_id': serviceId,
        'sampletype': sampletype,
        'department_code': departmentCode,
        'department': department,
        'reviews': reviews?.toJson(),
        'in_stock': inStock,
        'is_saved': isSaved,
        'is_wishlisted': isWishlisted,
        'is_item_in_cart': isItemInCart,
        'show_quantity_changer': showQuantityChanger,
      };
}
