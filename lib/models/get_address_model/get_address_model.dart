import 'datum.dart';

class GetAddressModel {
  List<Datum>? data;

  GetAddressModel({this.data});

  factory GetAddressModel.fromJson(Map<String, dynamic> json) {
    return GetAddressModel(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => Datum.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() => {
        'data': data?.map((e) => e.toJson()).toList(),
      };
}
