class Datum {
  int? id;
  String? firstName;
  String? lastName;
  dynamic companyName;
  dynamic vatId;
  String? address1;
  String? country;
  String? countryName;
  String? state;
  String? city;
  String? postcode;
  String? phone;
  bool? isDefault;
  String? createdAt;
  String? updatedAt;
  dynamic landmark;

  Datum({
    this.id,
    this.firstName,
    this.lastName,
    this.companyName,
    this.vatId,
    this.address1,
    this.country,
    this.countryName,
    this.state,
    this.city,
    this.postcode,
    this.phone,
    this.isDefault,
    this.createdAt,
    this.updatedAt,
    this.landmark,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json['id'] as int?,
        firstName: json['first_name'] as String?,
        lastName: json['last_name'] as String?,
        companyName: json['company_name'] as dynamic?,
        vatId: json['vat_id'] as dynamic?,
        address1: json['address1'] as String?,
        country: json['country'] as String?,
        countryName: json['country_name'] as String?,
        state: json['state'] as String?,
        city: json['city'] as String?,
        postcode: json['postcode'] as String?,
        phone: json['phone'] as String?,
        isDefault: json['is_default'] as bool?,
        createdAt: json['created_at'] as String?,
        updatedAt: json['updated_at'] as String?,
        landmark: json['landmark'] as dynamic?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'first_name': firstName,
        'last_name': lastName,
        'company_name': companyName,
        'vat_id': vatId,
        'address1': address1,
        'country': country,
        'country_name': countryName,
        'state': state,
        'city': city,
        'postcode': postcode,
        'phone': phone,
        'is_default': isDefault,
        'created_at': createdAt,
        'updated_at': updatedAt,
        'landmark': landmark,
      };
}
