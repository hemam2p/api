class ImageApi {
  String? message;
  String? status;

  ImageApi({this.message, this.status});

  factory ImageApi.fromJson(Map<String, dynamic> json) => ImageApi(
        message: json['message'] as String?,
        status: json['status'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'message': message,
        'status': status,
      };
}
