class OtpRegistration {
  String? message;

  OtpRegistration({this.message});

  factory OtpRegistration.fromJson(Map<String, dynamic> json) {
    return OtpRegistration(
      message: json['message'] as String?,
    );
  }

  Map<String, dynamic> toJson() => {
        'message': message,
      };
}
