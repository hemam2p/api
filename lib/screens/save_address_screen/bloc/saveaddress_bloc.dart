import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

import 'package:sample_api/base_equatable.dart';

import 'package:sample_api/models/savedatamodel/savedatamodel.dart';
import 'package:sample_api/respository/otp_registration_response.dart';

part 'saveaddress_event.dart';
part 'saveaddress_state.dart';

class SaveaddressBloc extends Bloc<SaveaddressEvent, SaveaddressState> {
  // late Data data;
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  SaveaddressBloc() : super(SaveaddressInitial()) {
    on<SaveaddressEvent>((event, emit) async {
      // TODO: implement event handler
      if (event is SaveaddressButtonEvent) {
        emit.call(SaveaddressLoadingstate());

        Savedatamodel savedatamodel = await saveAddress(
            firstNameController.text,
            lastNameController.text,
            emailController.text,
            addressController.text);

        emit.call(SaveaddressSuccessstate());

        print(savedatamodel);
      }
      print(firstNameController.text);
      print(lastNameController.text);
      print(emailController.text);
      print(addressController.text);
    });
  }
}
