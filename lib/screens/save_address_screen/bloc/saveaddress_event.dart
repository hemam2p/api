part of 'saveaddress_bloc.dart';

@immutable
abstract class SaveaddressEvent extends BaseEquatable {}

class SaveaddressIntialEvent extends SaveaddressEvent {}

class SaveaddressButtonEvent extends SaveaddressEvent {}
