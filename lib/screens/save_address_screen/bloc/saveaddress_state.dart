part of 'saveaddress_bloc.dart';

@immutable
abstract class SaveaddressState {}

class SaveaddressInitial extends SaveaddressState {}

class SaveaddressLoadedstate extends SaveaddressState {}

class SaveaddressLoadingstate extends SaveaddressState {}

class SaveaddressSuccessstate extends SaveaddressState {}

class SaveaddressFailurestate extends SaveaddressState {}
