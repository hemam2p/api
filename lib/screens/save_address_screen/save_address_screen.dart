import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sample_api/screens/customer_details/customer_details.dart';
import 'package:sample_api/screens/save_address_screen/bloc/saveaddress_bloc.dart';

class SaveAddressScreen extends StatefulWidget {
  const SaveAddressScreen({Key? key}) : super(key: key);

  @override
  _SaveAddressScreenState createState() => _SaveAddressScreenState();
}

class _SaveAddressScreenState extends State<SaveAddressScreen> {
  late SaveaddressBloc saveaddressBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    saveaddressBloc = SaveaddressBloc()..add(SaveaddressIntialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocListener(
            bloc: saveaddressBloc,
            listener: (BuildContext context, SaveaddressState state) {},
            child: BlocBuilder(
                bloc: saveaddressBloc,
                builder: (BuildContext context, SaveaddressState state) {
                  if (state is SaveaddressLoadingstate) {
                    return const Center(child: CircularProgressIndicator());
                  } else {
                    return Container(
                      margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextField(
                              controller: saveaddressBloc.firstNameController,
                              decoration: InputDecoration(
                                labelText: 'First Name',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            SizedBox(height: 10),
                            TextField(
                              controller: saveaddressBloc.lastNameController,
                              decoration: InputDecoration(
                                labelText: 'Last Name',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            SizedBox(height: 10),
                            TextField(
                              controller: saveaddressBloc.emailController,
                              decoration: InputDecoration(
                                labelText: 'email',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            SizedBox(height: 10),
                            TextField(
                              controller: saveaddressBloc.addressController,
                              decoration: InputDecoration(
                                labelText: 'Address',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            SizedBox(height: 10),
                            ElevatedButton(
                                onPressed: () {
                                  saveaddressBloc.add(SaveaddressButtonEvent());
                                  if (state is SaveaddressSuccessstate) {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                CustomerDetailsScreen()));
                                  }
                                },
                                child: Text('Save Address'))
                          ]),
                    );
                  }
                })));
  }
}
