part of 'imageapi_bloc.dart';

@immutable
abstract class ImageapiState {}

class ImageapiInitial extends ImageapiState {}

class ImageapiLoadedState extends ImageapiState {}

class ImageapiLoadingState extends ImageapiState {}

class ImageapiFailureState extends ImageapiState {}

class ImageapiSuccessState extends ImageapiState {}
