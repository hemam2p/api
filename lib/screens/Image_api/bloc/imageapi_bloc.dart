import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sample_api/base_equatable.dart';
import 'package:sample_api/models/image_api.dart';

import 'package:sample_api/respository/otp_registration_response.dart';

part 'imageapi_event.dart';
part 'imageapi_state.dart';

class ImageapiBloc extends Bloc<ImageapiEvent, ImageapiState> {
  dynamic networkImage;
  ImageapiBloc() : super(ImageapiInitial()) {
    on<ImageapiEvent>((event, emit) async {
      if (event is ImageApiIntialEvent) {
        emit.call(ImageapiLoadingState());
        Map<String, dynamic> response = await getImage();
        ImageApi imageResponse = ImageApi.fromJson(response);
        networkImage = imageResponse.message;
        print(imageResponse);
        print(imageResponse.message);
        print(imageResponse.status);
        emit.call(ImageapiSuccessState());
      }
    });
  }
}
