import 'package:flutter/material.dart';
import 'package:sample_api/screens/Image_api/bloc/imageapi_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sample_api/screens/sample_post_api/sample_post_api_screen.dart';

class ImageApiScreen extends StatefulWidget {
  const ImageApiScreen({Key? key}) : super(key: key);

  @override
  _ImageApiScreenState createState() => _ImageApiScreenState();
}

class _ImageApiScreenState extends State<ImageApiScreen> {
  late ImageapiBloc imageapiBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    imageapiBloc = ImageapiBloc()..add(ImageApiIntialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Image API'),
          actions: [
            IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SamplePostApiScreen()));
              },
              icon: Icon(Icons.arrow_forward_ios),
            ),
          ],
        ),
        body: BlocListener(
            bloc: imageapiBloc,
            listener: (BuildContext context, ImageapiState state) {},
            child: BlocBuilder(
                bloc: imageapiBloc,
                builder: (BuildContext context, ImageapiState state) {
                  if (state is ImageapiLoadingState) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    if (state is ImageapiSuccessState) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Center(
                            child: Image.network(
                              imageapiBloc.networkImage,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      );
                    }
                  }
                  return Container();
                })));
  }
}
