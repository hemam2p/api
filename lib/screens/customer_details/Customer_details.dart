import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sample_api/models/customer_details/data.dart';
import 'package:sample_api/screens/customer_details/bloc/customerdetails_bloc.dart';
import 'package:sample_api/screens/pagination_api/pagination_Api_screen.dart';

class CustomerDetailsScreen extends StatefulWidget {
  const CustomerDetailsScreen({Key? key}) : super(key: key);

  @override
  _CustomerDetailsScreenState createState() => _CustomerDetailsScreenState();
}

class _CustomerDetailsScreenState extends State<CustomerDetailsScreen> {
  late CustomerdetailsBloc customerdetailsBloc;
  List<Data>? data;
  get index => null;
  @override
  void initState() {
    super.initState();
    customerdetailsBloc = CustomerdetailsBloc()
      ..add(CustomerDetailsUIntialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
        child: ElevatedButton(
            onPressed: () {
              customerdetailsBloc.add(CustomerDetailsButtonEvent());
            },
            child: const Text('Fetch Data')),
      ),
      appBar: AppBar(
        title: const Text('Profile'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PaginationApiScreen()));
            },
            icon: Icon(Icons.arrow_forward_ios),
          ),
        ],
      ),
      body: BlocListener(
          bloc: customerdetailsBloc,
          listener: (BuildContext context, CustomerdetailsState state) {},
          child: BlocBuilder(
              bloc: customerdetailsBloc,
              builder: (BuildContext context, CustomerdetailsState state) {
                if (state is CustomerdetailsLoadingState) {
                  return const Center(child: CircularProgressIndicator());
                } else {
                  if (state is CustomerdetailsSuccessState) {
                    return Column(children: [
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Row(
                          children: [
                            Text('Name'),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                                '${customerdetailsBloc.customerDetailsmodel?.data!.firstName}'),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Row(
                          children: [
                            Text('Last name'),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                                '${customerdetailsBloc.customerDetailsmodel?.data!.lastName}'),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Row(
                          children: [
                            Text('Email'),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                                '${customerdetailsBloc.customerDetailsmodel?.data!.email}'),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Row(
                          children: [
                            Text('gender'),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                                '${customerdetailsBloc.customerDetailsmodel?.data!.gender}'),
                          ],
                        ),
                      ),
                    ]);
                  }
                }
                return Container();
              })),
    );
  }
}
