part of 'customerdetails_bloc.dart';

@immutable
abstract class CustomerdetailsState {}

class CustomerdetailsInitial extends CustomerdetailsState {}

class CustomerdetailsLoadedState extends CustomerdetailsState {}

class CustomerdetailsLoadingState extends CustomerdetailsState {}

class CustomerdetailsSuccessState extends CustomerdetailsState {}

class CustomerdetailsFailureState extends CustomerdetailsState {}
