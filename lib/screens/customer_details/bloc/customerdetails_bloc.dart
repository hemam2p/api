import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:sample_api/base_equatable.dart';
import 'package:sample_api/models/customer_details/customer_details.dart';
import 'package:sample_api/models/customer_details/data.dart';
import 'package:sample_api/respository/otp_registration_response.dart';

part 'customerdetails_event.dart';
part 'customerdetails_state.dart';

class CustomerdetailsBloc
    extends Bloc<CustomerdetailsEvent, CustomerdetailsState> {
  CustomerDetailsModel? customerDetailsmodel;

  CustomerdetailsBloc() : super(CustomerdetailsInitial()) {
    on<CustomerdetailsEvent>((event, emit) async {
      if (event is CustomerDetailsButtonEvent) {
        emit.call(CustomerdetailsLoadingState());
        var jsonResponse = await customerrDetails();

        customerDetailsmodel =
            CustomerDetailsModel.fromJson(jsonResponse as Map<String, dynamic>);

        print(jsonResponse);

        emit.call(CustomerdetailsSuccessState());
      } else {
        print('error');
      }
    });
  }
}
