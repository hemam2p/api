part of 'customerdetails_bloc.dart';

@immutable
abstract class CustomerdetailsEvent extends BaseEquatable {}

class CustomerDetailsUIntialEvent extends CustomerdetailsEvent {}

class CustomerDetailsButtonEvent extends CustomerdetailsEvent {}
