part of 'getallproducts_bloc.dart';

@immutable
abstract class GetallproductsState {}

class GetallproductsInitial extends GetallproductsState {}

class GetallproductsLoadedState extends GetallproductsState {}

class GetallproductsLoadingState extends GetallproductsState {}

class GetallproductsSuccessState extends GetallproductsState {}

class GetallproductsFailureState extends GetallproductsState {}
