import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sample_api/base_equatable.dart';
import 'package:sample_api/models/get_all_products_model/get_all_products_model.dart';
import 'package:sample_api/respository/otp_registration_response.dart';
import 'package:sample_api/screens/get_address_screen/bloc/getaddress_bloc.dart';

part 'getallproducts_event.dart';
part 'getallproducts_state.dart';

class GetallproductsBloc
    extends Bloc<GetallproductsEvent, GetallproductsState> {
  GetallproductsBloc() : super(GetallproductsInitial()) {
    int pageNo = 1;
    bool isFetching = false;
    on<GetallproductsEvent>((event, emit) async {
      // TODO: implement event handler
      if (event is GetAllProductsIntialEvent) {
        Map<String, dynamic> productResponseData = await getAllProducts(pageNo);
//  {[{array with nested parse}]}

        GetAllProductsModel getAllProductsModel =
            GetAllProductsModel.fromJson(productResponseData);
        print(productResponseData);
      }
    });
  }
}
