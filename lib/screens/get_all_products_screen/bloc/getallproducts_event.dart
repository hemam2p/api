part of 'getallproducts_bloc.dart';

@immutable
abstract class GetallproductsEvent extends BaseEquatable {}

class GetAllProductsIntialEvent extends GetallproductsEvent {}
