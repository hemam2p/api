import 'package:flutter/material.dart';
import 'package:sample_api/screens/get_all_products_screen/bloc/getallproducts_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GetAllproductsScreen extends StatefulWidget {
  const GetAllproductsScreen({Key? key}) : super(key: key);

  @override
  _GetAllproductsScreenState createState() => _GetAllproductsScreenState();
}

class _GetAllproductsScreenState extends State<GetAllproductsScreen> {
  late GetallproductsBloc getallproductsBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getallproductsBloc = GetallproductsBloc()..add(GetAllProductsIntialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocListener(
            bloc: getallproductsBloc,
            listener: (BuildContext context, GetallproductsState state) {},
            child: BlocBuilder(
                bloc: getallproductsBloc,
                builder: (BuildContext context, GetallproductsState state) {
                  return Column(
                    children: [],
                  );
                })));
  }
}
