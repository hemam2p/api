import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:sample_api/base_equatable.dart';
import 'package:sample_api/pagination_apimodel/market.dart';
import 'package:sample_api/pagination_apimodel/pagination_apimodel.dart';
import 'package:sample_api/respository/otp_registration_response.dart';

part 'paginationapi_event.dart';
part 'paginationapi_state.dart';

class PaginationapiBloc extends Bloc<PaginationapiEvent, PaginationapiState> {
  int pageNo = 0;
  bool isFirstLoading = false;
  bool isLoading = false;
  bool hasNextPage = true;
  List<Market> marketList = [];

  PaginationapiBloc() : super(PaginationapiInitial()) {
    on<PaginationapiEvent>((event, emit) async {
      // TODO: implement event handler
      if (event is PaginationApiIntialEvent) {
        emit.call(PaginationapiLoadingState());
        isFirstLoading = true;
        Map<String, dynamic> loadedResponse = await getListPage(pageNo);
        PaginationApimodel paginationApimodel =
            PaginationApimodel.fromJson(loadedResponse);
        marketList = paginationApimodel.markets ?? [];
        isFirstLoading = false;

        emit.call(PaginationapiSuccessState());
      }

      if (event is PaginationApiLoadedEvent) {
        emit.call(PaginationapiLoadingState());
        hasNextPage = true;
        Map<String, dynamic> loadedResponse = await getListPage(pageNo);
        PaginationApimodel paginationApimodel =
            PaginationApimodel.fromJson(loadedResponse);
        marketList.addAll(paginationApimodel.markets ?? []);

        emit.call(PaginationapiSuccessState());
        hasNextPage = false;
      }
    });
  }
}
