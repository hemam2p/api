part of 'paginationapi_bloc.dart';

@immutable
abstract class PaginationapiState {}

class PaginationapiInitial extends PaginationapiState {}

class PaginationapiLoadedState extends PaginationapiState {}

class PaginationapiLoadingState extends PaginationapiState {}

class PaginationapiSuccessState extends PaginationapiState {}

class PaginationapiFailureState extends PaginationapiState {}
