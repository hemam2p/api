part of 'paginationapi_bloc.dart';

@immutable
abstract class PaginationapiEvent extends BaseEquatable {}

class PaginationApiIntialEvent extends PaginationapiEvent {}

class PaginationApiLoadedEvent extends PaginationapiEvent {}
