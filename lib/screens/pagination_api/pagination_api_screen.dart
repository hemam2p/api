import 'package:flutter/material.dart';
import 'package:sample_api/screens/Image_api/image_api_screen.dart';
import 'package:sample_api/screens/pagination_api/bloc/paginationapi_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PaginationApiScreen extends StatefulWidget {
  const PaginationApiScreen({Key? key}) : super(key: key);

  @override
  _PaginationApiScreenState createState() => _PaginationApiScreenState();
}

class _PaginationApiScreenState extends State<PaginationApiScreen> {
  late PaginationapiBloc paginationapiBloc;
  ScrollController scrollController = ScrollController();

  void _loadMore() async {
    if (paginationapiBloc.hasNextPage == true &&
        paginationapiBloc.isLoading == false &&
        paginationapiBloc.isFirstLoading == false) {
      if (scrollController.position.extentAfter < 250) {
        setState(() {
          paginationapiBloc.isLoading = true;
        });
        paginationapiBloc.pageNo += 1;
        paginationapiBloc.add(PaginationApiLoadedEvent());
        paginationapiBloc.isLoading = false;
      } else {
        setState(() {
          paginationapiBloc.hasNextPage = false;
        });
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    paginationapiBloc = PaginationapiBloc()..add(PaginationApiIntialEvent());
    scrollController = ScrollController()..addListener(_loadMore);
  }

  @override
  void dispose() {
    scrollController.removeListener(_loadMore);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagination API'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ImageApiScreen()));
            },
            icon: Icon(Icons.arrow_forward_ios),
          ),
        ],
      ),
      body: BlocListener(
          bloc: paginationapiBloc,
          listener: (BuildContext context, PaginationapiState state) {},
          child: BlocBuilder(
              bloc: paginationapiBloc,
              builder: (BuildContext context, PaginationapiState state) {
                if (state is PaginationapiLoadingState) {
                  paginationapiBloc.isLoading == true;
                  return const Center(child: CircularProgressIndicator());
                } else {
                  if (state is PaginationapiSuccessState) {
                    return ListView.separated(
                      controller: scrollController,
                      itemBuilder: (BuildContext context, int index) {
                        print(index);
                        return ListTile(
                          title: Text(
                              '${paginationapiBloc.marketList[index].exchangeId}'),
                          subtitle: Text(
                              '${paginationapiBloc.marketList[index].quoteAsset}'),
                          trailing: Text(
                              '${paginationapiBloc.marketList[index].price}'),
                        );
                      },
                      separatorBuilder: (context, index) => const Divider(
                        thickness: 1,
                      ),
                      itemCount: paginationapiBloc.marketList.length,
                    );
                  }
                }
                return Container();
              })),
    );
  }
}
