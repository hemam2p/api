import 'package:flutter/material.dart';
import 'package:sample_api/screens/sample_post_api/bloc/samplepostapi_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SamplePostApiScreen extends StatefulWidget {
  const SamplePostApiScreen({Key? key}) : super(key: key);

  @override
  _SamplePostApiScreenState createState() => _SamplePostApiScreenState();
}

class _SamplePostApiScreenState extends State<SamplePostApiScreen> {
  late SamplepostapiBloc samplepostapiBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    samplepostapiBloc = SamplepostapiBloc()..add(SamplePostApiIntialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
          margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
          child: ElevatedButton(
              onPressed: () {
                samplepostapiBloc.add(SamplePostApiIntialEvent());
              },
              child: Text('Id has been Sent'))),
      appBar: AppBar(
        title: const Text('Sample Post API'),
      ),
      body: BlocListener(
          bloc: samplepostapiBloc,
          listener: (BuildContext context, SamplepostapiState state) {},
          child: BlocBuilder(
              bloc: samplepostapiBloc,
              builder: (BuildContext context, SamplepostapiState state) {
                if (state is SamplepostapiLoadingState) {
                  return const Center(child: CircularProgressIndicator());
                } else {
                  if (state is SamplepostapiSuccessState) {
                    return Column(
                      children: [],
                    );
                  }
                }
                return Container();
              })),
    );
  }
}
