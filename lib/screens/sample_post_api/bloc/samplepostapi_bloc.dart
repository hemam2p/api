import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sample_api/base_equatable.dart';
import 'package:sample_api/models/sample_post_api_model.dart';
import 'package:sample_api/respository/otp_registration_response.dart';

part 'samplepostapi_event.dart';
part 'samplepostapi_state.dart';

class SamplepostapiBloc extends Bloc<SamplepostapiEvent, SamplepostapiState> {
  int? id;
  SamplepostapiBloc() : super(SamplepostapiInitial()) {
    on<SamplepostapiEvent>((event, emit) async {
      // TODO: implement event handler
      if (event is SamplePostApiIntialEvent) {
        Map<String, dynamic> getID = await getId(id = 200);
        SamplePostApiModel samplePostApiModel =
            SamplePostApiModel.fromJson(getID);

        print(samplePostApiModel.id);
      }
    });
  }
}
