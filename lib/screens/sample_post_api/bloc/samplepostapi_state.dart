part of 'samplepostapi_bloc.dart';

@immutable
abstract class SamplepostapiState {}

class SamplepostapiInitial extends SamplepostapiState {}

class SamplepostapiLoadedstate extends SamplepostapiState {}

class SamplepostapiLoadingState extends SamplepostapiState {}

class SamplepostapiSuccessState extends SamplepostapiState {}

class SamplepostapiFailureState extends SamplepostapiState {}
