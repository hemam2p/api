import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:sample_api/screens/get_address_screen/bloc/getaddress_bloc.dart';
import 'package:sample_api/screens/save_address_screen/save_address_screen.dart';

class GetAddressScreen extends StatefulWidget {
  const GetAddressScreen({Key? key}) : super(key: key);

  @override
  _GetAddressScreenState createState() => _GetAddressScreenState();
}

class _GetAddressScreenState extends State<GetAddressScreen> {
  late GetaddressBloc getaddressBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getaddressBloc = GetaddressBloc()..add(GetAddressIntialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocListener(
            bloc: getaddressBloc,
            listener: (BuildContext context, GetaddressState state) {},
            child: BlocBuilder(
                bloc: getaddressBloc,
                builder: (BuildContext context, GetaddressState state) {
                  if (state is GetAddressLoadingState) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    if (state is GetAddressSuccessState) {
                      return Column(
                        children: [
                          Expanded(
                            flex: 1,
                            child: ListView.builder(
                                itemCount: getaddressBloc.addressList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return ListTile(
                                      title: Text(
                                          'Name :${getaddressBloc.addressList[index].firstName}' +
                                              ' ' +
                                              '${getaddressBloc.addressList[index].lastName}'),
                                      subtitle: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('Address :${getaddressBloc.addressList[index].address1}' +
                                              ' , ' +
                                              '${getaddressBloc.addressList[index].city}' +
                                              ',' +
                                              '${getaddressBloc.addressList[index].state}'),
                                          Text(
                                              'Pincode:${getaddressBloc.addressList[index].postcode}'),
                                        ],
                                      ));
                                }),
                          ),
                          ElevatedButton(
                              onPressed: () {
                                if (state is GetAddressSuccessState) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              SaveAddressScreen()));
                                }
                              },
                              child: Text('Resposne')),
                        ],
                      );
                    }
                  }
                  return Container();
                })));
  }
}
