part of 'getaddress_bloc.dart';

@immutable
abstract class GetaddressEvent extends BaseEquatable {}

class GetAddressIntialEvent extends GetaddressEvent {}
