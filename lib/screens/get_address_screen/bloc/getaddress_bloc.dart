import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sample_api/base_equatable.dart';
import 'package:sample_api/models/get_address_model/datum.dart';
import 'package:sample_api/models/get_address_model/get_address_model.dart';
import 'package:sample_api/respository/otp_registration_response.dart';

part 'getaddress_event.dart';
part 'getaddress_state.dart';

class GetaddressBloc extends Bloc<GetaddressEvent, GetaddressState> {
  List<Datum> addressList = [];
  GetaddressBloc() : super(GetaddressInitial()) {
    on<GetaddressEvent>((event, emit) async {
      if (event is GetAddressIntialEvent) {
        emit.call(GetAddressLoadingState());
        Map<String, dynamic> addressResponseData = await getAddress();
//  {[{array with nested parse}]}

        GetAddressModel getAddressModel =
            GetAddressModel.fromJson(addressResponseData);
        addressList = getAddressModel.data ?? [];

        print(getAddressModel.data![0].city);
        print(getAddressModel.data![1].country);

        emit.call(GetAddressSuccessState());
        print('success');
      } else {
        print('error');
      }
    });
  }
}
