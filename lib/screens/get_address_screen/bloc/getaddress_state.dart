part of 'getaddress_bloc.dart';

@immutable
abstract class GetaddressState {}

class GetaddressInitial extends GetaddressState {}

class GetAddressLoadedState extends GetaddressState {}

class GetAddressLoadingState extends GetaddressState {}

class GetAddressSuccessState extends GetaddressState {}

class GetAddressFailureState extends GetaddressState {}
