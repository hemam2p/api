part of 'otpregistration_bloc.dart';

@immutable
abstract class OtpregistrationState {}

class OtpregistrationInitial extends OtpregistrationState {}

class OtpregistrationLoadingState extends OtpregistrationState {}

class OtpregistrationLoadedState extends OtpregistrationState {}

class OtpregistrationSuccessState extends OtpregistrationState {}

class OtpregistrationFailureState extends OtpregistrationState {}

class OtpregistrationNavigationState extends OtpregistrationState {}
