import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:sample_api/base_equatable.dart';

import 'package:sample_api/respository/otp_registration_response.dart';
import 'package:sample_api/screens/get_address_screen/get_address_screen.dart';

part 'otpregistration_event.dart';
part 'otpregistration_state.dart';

class OtpregistrationBloc
    extends Bloc<OtpregistrationEvent, OtpregistrationState> {
  TextEditingController mobileNumberController = TextEditingController();
  String errorMessage = '';

  OtpregistrationBloc() : super(OtpregistrationInitial()) {
    // ignore: void_checks
    on<OtpregistrationEvent>((event, emit) async {
      if (event is OtpButtonEvent) {
        emit.call(OtpregistrationLoadingState());
        if (mobileNumberController.text.isEmpty) {
          errorMessage = 'Enter the number';
        } else {
          var otpRegistration = await otpResponse(mobileNumberController.text);
          emit.call(OtpregistrationSuccessState());

          print(otpRegistration);
        }
      }
    });
  }
}
