part of 'otpregistration_bloc.dart';

@immutable
abstract class OtpregistrationEvent extends BaseEquatable {}

class OtpRegistrationIntialEvent extends OtpregistrationEvent {}

class OtpButtonEvent extends OtpregistrationEvent {}
