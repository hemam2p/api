import 'package:flutter/material.dart';
// import 'package:sample_api/screens/bloc/otpregistration_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sample_api/screens/customer_details/customer_details.dart';
import 'package:sample_api/screens/get_address_screen/get_address_screen.dart';
import 'package:sample_api/screens/otp_registration/bloc/otpregistration_bloc.dart';

class OtpRegistration extends StatefulWidget {
  const OtpRegistration({Key? key}) : super(key: key);

  @override
  _OtpRegistrationState createState() => _OtpRegistrationState();
}

class _OtpRegistrationState extends State<OtpRegistration> {
  final _formKey = GlobalKey<FormState>();
  late OtpregistrationBloc otpregistrationBloc;
  @override
  void initState() {
    super.initState();
    otpregistrationBloc = OtpregistrationBloc()
      ..add(OtpRegistrationIntialEvent());
    // otpregistrationBloc = BlocProvider.of<OtpregistrationBloc>(context)
    //   ..add(OtpRegistrationIntialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocListener(
      bloc: otpregistrationBloc,
      listener: (BuildContext context, OtpregistrationState state) {
        // if (state is OtpregistrationLoadingState) {
        //   Navigator.push(context,
        //       MaterialPageRoute(builder: (context) => const CustomerDetails()));
        // }
      },
      child: BlocBuilder(
          bloc: otpregistrationBloc,
          builder: (BuildContext context, OtpregistrationState state) {
            return Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Form(
                      key: _formKey,
                      child: TextFormField(
                        controller: otpregistrationBloc.mobileNumberController,
                        decoration: InputDecoration(
                          labelText: 'Mobile Number',
                          border: OutlineInputBorder(),
                        ),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        if (otpregistrationBloc
                            .mobileNumberController.text.isEmpty) {
                          print(otpregistrationBloc.errorMessage =
                              'Enter the number');
                        } else {
                          otpregistrationBloc.add(OtpButtonEvent());
                        }
                        if (state is OtpregistrationSuccessState) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GetAddressScreen()));
                        }
                      },
                      child: const Text('Send OTP')),
                ],
              ),
            );
            // return Container();
          }),
    )
        // })),
        );
  }
}
