import 'package:dio/dio.dart';

// import 'package:sample_api/models/customer_details/customer_details.dart';
// import 'package:sample_api/models/customer_details/data.dart';
// import 'package:sample_api/models/get_address_model/get_address_model.dart';
// import 'package:sample_api/models/image_api.dart';
// import 'package:sample_api/models/savedatamodel/billing_address.dart';
import 'package:sample_api/models/savedatamodel/savedatamodel.dart';
// import 'package:sample_api/pagination_apimodel/pagination_apimodel.dart';

import '../models/otp_registration.dart';

Future otpResponse(String mobileno) async {
  var dio = Dio();

  Response response;
  try {
    var formData = FormData.fromMap({
      'phone': mobileno,
    });
    response = await dio.post(
        'http://markhealth.quadkeys.in/api/customer/signup',
        data: formData);

    if (response.statusCode == 200) {
      return response.data;
    } else {
      if (response.statusCode == 400) {}
      throw Exception('Failed to load ');
    }

    // return OtpRegistration.fromJson(response.data);
  } on DioError catch (e) {
    return OtpRegistration.fromJson({"error": e.error});
  }
}

Future<dynamic> customerrDetails() async {
  var dio = Dio();
  Response response;
  try {
    response = await dio.get(
        'http://markhealth.quadkeys.in/api/customer/get?token=true',
        queryParameters: {"token": true},
        options: Options(headers: {
          "Authorization":
              'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9tYXJraGVhbHRoLnF1YWRrZXlzLmluXC9hcGlcL290cExvZ2luIiwiaWF0IjoxNjM3NTc1Mzk3LCJuYmYiOjE2Mzc1NzUzOTcsImp0aSI6ImFoM2IzRUJCOHJmajFNd1AiLCJzdWIiOjU4LCJwcnYiOiI4ZmNhMDg4YWJhZTJmOWE4Zjg0YTVmMGJmNmE2NTI0NDkwNTViZTAwIn0.zFVDJpUa9sQPpN37t8N_4C6g89ePKuztEKkr0OKeCio',
          "Accept": "application/json",
        }));

    print(response.data);
    if (response.statusCode == 200) {
      return response.data;
    }
  } on DioError catch (e) {
    print('hi');
    print(e.error);
  }
}

Future getAddress() async {
  var dio = Dio();
  Response response;
  try {
    response =
        await dio.get('http://markhealth.quadkeys.in/api/addresses?token=true',
            queryParameters: {"token": true},
            options: Options(headers: {
              "Authorization":
                  'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9tYXJraGVhbHRoLnF1YWRrZXlzLmluXC9hcGlcL290cExvZ2luIiwiaWF0IjoxNjM3NTc1Mzk3LCJuYmYiOjE2Mzc1NzUzOTcsImp0aSI6ImFoM2IzRUJCOHJmajFNd1AiLCJzdWIiOjU4LCJwcnYiOiI4ZmNhMDg4YWJhZTJmOWE4Zjg0YTVmMGJmNmE2NTI0NDkwNTViZTAwIn0.zFVDJpUa9sQPpN37t8N_4C6g89ePKuztEKkr0OKeCio',
              "Accept": 'application/json',
            }));
    print(response.data);
    if (response.statusCode == 200) {
      // json.decode(response.data);
      return response.data;
    }
    return {"success": true, "data": response.data};
  } on DioError catch (e) {
    return {"success": false, "data": e.toString()};
  }
}

Future getAllProducts(int pageNo) async {
  var dio = Dio();
  Response response;
  try {
    response = await dio.get(
      'http://markhealth.quadkeys.in/api/products',
      queryParameters: {"limit": 10, "page": pageNo, "category_id": 2},
    );
    print(response.data);
    if (response.statusCode == 200) {
      return response.data;
    }
    return {"success": true, "data": response.data};
  } on DioError catch (e) {
    return {"success": false, "data": e.toString()};
  }
}

Future<Savedatamodel> saveAddress(
    String firstName, String lastName, String email, String address) async {
  var dio = Dio();

  try {
    Response response = await dio.post(
        'http://markhealth.quadkeys.in/api/checkout/save-address',
        queryParameters: {"token": true},
        data: {
          "billing": {
            "address1": {"0": address},
            "use_for_shipping": "true",
            "email": email,
            "first_name": firstName,
            "last_name": lastName,
          }
        },
        options: Options(headers: {
          "Authorization":
              'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9tYXJraGVhbHRoLnF1YWRrZXlzLmluXC9hcGlcL290cExvZ2luIiwiaWF0IjoxNjM3NTc1Mzk3LCJuYmYiOjE2Mzc1NzUzOTcsImp0aSI6ImFoM2IzRUJCOHJmajFNd1AiLCJzdWIiOjU4LCJwcnYiOiI4ZmNhMDg4YWJhZTJmOWE4Zjg0YTVmMGJmNmE2NTI0NDkwNTViZTAwIn0.zFVDJpUa9sQPpN37t8N_4C6g89ePKuztEKkr0OKeCio',
        }));

    if (response.statusCode == 200) {
      return response.data;
    }
    return Savedatamodel.fromJson(response.data);
  } on DioError catch (e) {
    return Savedatamodel.fromJson({"error": e.error});
  }
}

Future<Map<String, dynamic>> getImage() async {
  var dio = Dio();
  try {
    Response response = await dio.get(
      'https://dog.ceo/api/breeds/image/random',
    );
    if (response.statusCode == 200) {
      print(response.data);
      return response.data;
    }
    return response.data;
  } on DioError catch (e) {
    return {"success": false, "data": e.toString()};
  }
}

Future<Map<String, dynamic>> getListPage(int pageNum) async {
  var dio = Dio();
  try {
    Response response = await dio.get('https://www.cryptingup.com/api/markets',
        queryParameters: {"size": 10, "start": pageNum});
    if (response.statusCode == 200) {
      print(response.data);
      return response.data;
    }
    return response.data;
  } on DioError catch (e) {
    return {"success": false, "data": e.toString()};
  }
}

Future<Map<String, dynamic>> getId(int iD) async {
  var dio = Dio();
  try {
    Response response = await dio
        .post('https://jsonplaceholder.typicode.com/posts', data: {"id": iD});
    if (response.statusCode == 201) {
      print(response.data);
      return response.data;
    }
    return response.data;
  } on DioError catch (e) {
    return {"success": false, "data": e.toString()};
  }
}
